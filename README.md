# Hello DevOps

Sample WebApp to hello DevOps

## Build the project

If you have maven installed simply execute `mvn clean install` to run the project.

## Running the project

If you have maven installed simply execute `mvn spring-boot:run` to run the project.

