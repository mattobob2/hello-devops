package de.hello.devops;

import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;

@SpringBootApplication
public class HelloDevOps {

    public static void main(String[] args) {
        SpringApplication.run(HelloDevOps.class, args);
    }

}

