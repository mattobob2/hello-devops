package de.hello.devops.controllers;

import de.hello.devops.services.SenderService;
import lombok.RequiredArgsConstructor;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.ResponseBody;
import org.springframework.web.bind.annotation.RestController;

@RequiredArgsConstructor(onConstructor = @__(@Autowired))
@RestController
public class SenderController {

    private final SenderService senderService;

    @GetMapping(path = "/message")
    @ResponseBody
    public String sendMessage(String message) {
        return senderService.sendTestMessage(message);
    }

}
