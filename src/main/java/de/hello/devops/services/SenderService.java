package de.hello.devops.services;

import lombok.RequiredArgsConstructor;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.core.env.Environment;
import org.springframework.stereotype.Service;
import org.springframework.util.StreamUtils;

import javax.annotation.PostConstruct;
import java.io.IOException;
import java.nio.charset.StandardCharsets;
import java.util.Arrays;

@RequiredArgsConstructor(onConstructor = @__(@Autowired))
@Service
public class SenderService {

    private final Environment environment;

    private String response;

    @PostConstruct
    void init() throws IOException {
        response = StreamUtils
                .copyToString(getClass()
                        .getClassLoader()
                        .getResourceAsStream("dynamic/hello.html"), StandardCharsets.UTF_8);

        if (Arrays.stream(environment.getActiveProfiles()).anyMatch(s -> "prod".equals(s))) {
            response = response.replace("DEV", "PROD");
        }
    }

    public String sendTestMessage(String payload) {
        return response;
    }

}


