package de.hello.devops;

import com.tngtech.archunit.core.domain.JavaClasses;
import com.tngtech.archunit.core.importer.ClassFileImporter;
import com.tngtech.archunit.library.GeneralCodingRules;
import com.tngtech.archunit.library.dependencies.SlicesRuleDefinition;
import org.junit.jupiter.api.BeforeEach;
import org.junit.jupiter.api.Test;

public class ArchitectureTest {
    private JavaClasses classes;

    @BeforeEach
    public void setUp() throws Exception {
        classes = new ClassFileImporter().importPackages("de.allianz.dsm.prozesstransparenz");
    }

    @Test
    public void packagesShouldNotHaveCyclicDependencies() {
        SlicesRuleDefinition.slices().matching("de.allianz.dsm.prozesstransparenz.(**)..").should().beFreeOfCycles().check(classes);
    }

    @Test
    public void doNotUseStandardOutOrStandardError() {
        GeneralCodingRules.NO_CLASSES_SHOULD_ACCESS_STANDARD_STREAMS
                .because("logging (SLF4J) should be used instead")
                .check(classes);
    }
}
