package de.hello.devops.controllers;

import de.hello.devops.services.SenderService;
import org.junit.jupiter.api.Test;
import org.junit.jupiter.api.extension.ExtendWith;
import org.junit.platform.commons.util.StringUtils;
import org.mockito.InjectMocks;
import org.mockito.Mock;
import org.mockito.junit.jupiter.MockitoExtension;

import static org.junit.jupiter.api.Assertions.*;
import static org.mockito.Mockito.when;

@ExtendWith(MockitoExtension.class)
class SenderControllerTest {

    @InjectMocks
    private SenderController senderController;

    @Mock
    private SenderService senderService;


    private String testPayload = "testPayload";
    private String testSting = "testString";

    @Test
    void sendMessage_OK() {
        when(senderService.sendTestMessage("test")).thenReturn(testSting);

        assertTrue(StringUtils.isNotBlank(senderService.sendTestMessage("test")));
        assertTrue(StringUtils.isNotBlank(senderController.sendMessage("test")));
    }
}
