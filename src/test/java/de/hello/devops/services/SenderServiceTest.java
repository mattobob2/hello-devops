package de.hello.devops.services;

import org.junit.jupiter.api.Test;
import org.junit.jupiter.api.extension.ExtendWith;
import org.junit.platform.commons.util.StringUtils;
import org.mockito.InjectMocks;
import org.mockito.junit.jupiter.MockitoExtension;

import static org.junit.jupiter.api.Assertions.assertNotNull;
import static org.junit.jupiter.api.Assertions.assertTrue;


@ExtendWith(MockitoExtension.class)
class SenderServiceTest {

    @InjectMocks
    private SenderService senderService;

    private String testPayload = "testPayload";


    @Test
    void sendTestMessage_OK() {
        assertTrue(StringUtils.isBlank(senderService.sendTestMessage("test")));;
    }


}
